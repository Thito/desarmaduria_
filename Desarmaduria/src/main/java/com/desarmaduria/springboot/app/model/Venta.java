package com.desarmaduria.springboot.app.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="venta")
public class Venta {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private Integer codigoVenta;
	
	@ManyToOne
	@JoinColumn(name = "id_Vendedor")
	private Vendedor vendedor;
	
	private Date fecha;
	
	private String detalleVenta;
	
	@OneToOne
	@JoinColumn(name = "id_Cliente")
	private Cliente cliente;
	
	@OneToMany(cascade = CascadeType.ALL)
	private List<Articulo> articulos;

	public Venta(Integer codigoVenta, Vendedor vendedor, Date fecha, String detalleVenta, Cliente cliente,
			List<Articulo> articulos, int totalVenta) {
		this.codigoVenta = codigoVenta;
		this.vendedor = vendedor;
		this.fecha = fecha;
		this.detalleVenta = detalleVenta;
		this.cliente = cliente;
		this.articulos = articulos;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCodigoVenta() {
		return codigoVenta;
	}

	public void setCodigoVenta(Integer codigoVenta) {
		this.codigoVenta = codigoVenta;
	}

	public Vendedor getVendedor() {
		return vendedor;
	}

	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getDetalleVenta() {
		return detalleVenta;
	}

	public void setDetalleVenta(String detalleVenta) {
		this.detalleVenta = detalleVenta;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Articulo> getArticulos() {
		return articulos;
	}

	public void setArticulos(List<Articulo> articulos) {
		this.articulos = articulos;
	}
	
	public void addArticulo(Articulo articulo) {
		this.articulos.add(articulo);
	}
}
