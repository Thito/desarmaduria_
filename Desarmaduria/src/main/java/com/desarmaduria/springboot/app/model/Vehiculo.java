package com.desarmaduria.springboot.app.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="vehiculo")
public class Vehiculo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private String marca;
	private String modelo;
	private int año;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "id_Cliente3")
	private Cliente cliente;
	
	@ManyToMany
    @JoinTable(name = "mecanico_vehiculo", joinColumns = @JoinColumn(name = "vehiculo_id"),
    inverseJoinColumns = @JoinColumn(name = "mecanico_id"))
    List<Mecanico> mecanicos;
	
	@ManyToOne
	@JoinColumn(name = "id_Garage")
	private Garage garage;

	public Vehiculo(String marca, String modelo, int año, Cliente cliente, List<Mecanico> mecanicos, Garage garage) {
		this.marca = marca;
		this.modelo = modelo;
		this.año = año;
		this.cliente = cliente;
		this.mecanicos = mecanicos;
		this.garage = garage;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getAño() {
		return año;
	}

	public void setAño(int año) {
		this.año = año;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Mecanico> getMecanicos() {
		return mecanicos;
	}

	public void setMecanicos(List<Mecanico> mecanicos) {
		this.mecanicos = mecanicos;
	}

	public Garage getGarage() {
		return garage;
	}

	public void setGarage(Garage garage) {
		this.garage = garage;
	}
	
	
	

}
