package com.desarmaduria.springboot.app.service;

import java.util.List;

import com.desarmaduria.springboot.app.model.Vendedor;

public interface VendedorService {

	public List<Vendedor> findAllVendedores();

	public Vendedor findVendedorById(int idVendedor) throws DesarmaduriaException;

	public void save(Vendedor vendedor) throws DesarmaduriaException;

	public void deleteVendedorById(int idVendedor) throws DesarmaduriaException;


}
