package com.desarmaduria.springboot.app.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.desarmaduria.springboot.app.model.Vendedor;
import com.desarmaduria.springboot.app.service.DesarmaduriaException;
import com.desarmaduria.springboot.app.service.VendedorService;


@RestController
@RequestMapping("vendedores")
public class VendedorRestController {
	
	@Autowired
	private VendedorService vendedorService;
	
	@GetMapping(value = "", produces = "application/json")
	public ResponseEntity<List<Vendedor>> getAllVendedores() {
		List<Vendedor> lista = vendedorService.findAllVendedores();
		if (lista.isEmpty()) {
			return new ResponseEntity<List<Vendedor>>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<List<Vendedor>>(lista,HttpStatus.OK);
		}
	}
	
	@GetMapping(value="/{idVendedor}", produces="application/json")
	public ResponseEntity<Vendedor> getVendedorById(@PathVariable("idVendedor") int id) throws DesarmaduriaException {
		Vendedor vendedor = vendedorService.findVendedorById(id);
		if (vendedor == null) {
			return new ResponseEntity<Vendedor>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Vendedor>(vendedor, HttpStatus.OK);
		}
	}
	
	@PostMapping(value = "", produces = "application/json")
	public ResponseEntity<Vendedor> addVendedor(@RequestBody Vendedor vendedor) {
		try {
			vendedorService.save(vendedor);
			return new ResponseEntity<Vendedor>(vendedor, HttpStatus.CREATED);
		} catch (DesarmaduriaException e) {
			return new ResponseEntity<Vendedor>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value = "/{idVendedor}")
	public ResponseEntity<Vendedor> updateVendedor(@PathVariable("idVendedor") int idVendedor, @RequestBody Vendedor vendedor) throws DesarmaduriaException{
			Vendedor vendedor1 = vendedorService.findVendedorById(idVendedor);
			if (vendedor1 == null) {
				return new ResponseEntity<Vendedor>(HttpStatus.NOT_FOUND);
			}
			vendedor1.setNombre(vendedor.getNombre());
			vendedor1.setApellido(vendedor.getApellido());
			vendedor1.setTelefono(vendedor.getTelefono());
			vendedorService.save(vendedor1);
			return new ResponseEntity<Vendedor>(vendedor1, HttpStatus.NO_CONTENT);
	}
	
	@DeleteMapping(value="/{idVendedor}", produces="application/json")
	public ResponseEntity<Vendedor> deleteVendedorById(@PathVariable("idVendedor") int idVendedor) {
		try {
			vendedorService.deleteVendedorById(idVendedor);
			return new ResponseEntity<Vendedor>(HttpStatus.OK);	
		} catch (DesarmaduriaException e) {
			return new ResponseEntity<Vendedor>(HttpStatus.NOT_FOUND);
		}
	}
}
