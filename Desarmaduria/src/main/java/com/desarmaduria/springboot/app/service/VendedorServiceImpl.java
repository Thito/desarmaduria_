package com.desarmaduria.springboot.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.desarmaduria.springboot.app.model.Vendedor;
import com.desarmaduria.springboot.app.repository.VendedorRepository;

@Service
public class VendedorServiceImpl implements VendedorService{
	
	@Autowired
	private VendedorRepository vendedorRepository;
	
	@Override
	public List<Vendedor> findAllVendedores() {
		return vendedorRepository.findAll();
	}

	@Override
	public Vendedor findVendedorById(int idVendedor) throws DesarmaduriaException{
		Vendedor vendedor = vendedorRepository.findById(idVendedor);
		if (vendedor != null) {
			return vendedor;
		} else {
			throw new DesarmaduriaException();
		}
	}
	
	@Transactional
	@Override
	public void save(Vendedor vendedor) throws DesarmaduriaException{
		if (vendedor == null) {
			throw new DesarmaduriaException();
		}
		try {
			vendedorRepository.save(vendedor);
		} catch (DataAccessException e) {
			e.printStackTrace();
			throw new DesarmaduriaException();
		}
	}
	
	@Transactional
	@Override
	public void deleteVendedorById(int idVendedor) throws DesarmaduriaException {
		try {
			vendedorRepository.deleteById(idVendedor);	
		}
		catch (DataAccessException e) {
			throw new DesarmaduriaException();
		}		
		
	}
}
