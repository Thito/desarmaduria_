package com.desarmaduria.springboot.app.repository;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.desarmaduria.springboot.app.model.Articulo;

public interface ArticuloRepository {
	
	public List<Articulo> findAll() throws DataAccessException;
	
	public int stockByNombre(String nombre) throws DataAccessException;
	
	public Articulo findById(int id) throws DataAccessException;

	public void save(Articulo articulo) throws DataAccessException;

	public void deleteById(int id) throws DataAccessException;

	List<Articulo> articulosByBodega(String nombre);



}
