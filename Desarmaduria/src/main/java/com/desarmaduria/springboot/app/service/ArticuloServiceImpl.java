package com.desarmaduria.springboot.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.desarmaduria.springboot.app.model.Articulo;
import com.desarmaduria.springboot.app.model.Cliente;
import com.desarmaduria.springboot.app.repository.ArticuloRepository;



@Service
public class ArticuloServiceImpl implements ArticuloService {
	
	@Autowired
	private ArticuloRepository articuloRepository;

	@Override
	public List<Articulo> findAllArticulos() {
		// TODO Auto-generated method stub
		return articuloRepository.findAll();
		
	}
	
	@Override
	public int getStockByNombreArticulo(String nombre)  {
		return articuloRepository.stockByNombre(nombre);
	}
	
	
	@Override
	public List<Articulo> getArticulosPorBodega(String nomBodega){
		return articuloRepository.articulosByBodega(nomBodega);
		
	}

	@Override
	public Articulo findArticuloById(int idArticulo) throws DesarmaduriaException {
		// TODO Auto-generated method stub
		Articulo articulo = articuloRepository.findById(idArticulo);
		if (articulo != null) {
			return articulo;
		} else {
			throw new DesarmaduriaException();
		}
	}
	@Transactional
	@Override
	public void save(Articulo articulo) throws DesarmaduriaException {
		// TODO Auto-generated method stub
		if (articulo == null) {
			throw new DesarmaduriaException();
		}
		try {
			articuloRepository.save(articulo);
		} catch (DataAccessException e) {
			e.printStackTrace();
			throw new DesarmaduriaException();
		}
		

	}
	@Transactional
	@Override
	public void deleteArticuloById(int idArticulo) throws DesarmaduriaException {
		// TODO Auto-generated method stub
		
		try {
			articuloRepository.deleteById(idArticulo);	
		}
		catch (DataAccessException e) {
			throw new DesarmaduriaException();
		}		

	}

}
